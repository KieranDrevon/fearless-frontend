import React from "react";

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStarts = this.handleStarts.bind(this);
        this.handleEnds = this.handleEnds.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handleMaxPresentations = this.handleMaxPresentations.bind(this);
        this.handleMaxAttendees = this.handleMaxAttendees.bind(this);
        this.handleLocations = this.handleLocations.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: ''
            }
            this.setState(cleared);
        }

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name:value});
    }

    handleStarts(event) {
        const value = event.target.value;
        this.setState({starts:value});
    }

    handleEnds(event) {
        const value = event.target.value;
        this.setState({ends:value});
    }

    handleDescription(event) {
        const value = event.target.value;
        this.setState({description:value});
    }

    handleMaxPresentations(event) {
        const value = event.target.value;
        this.setState({maxPresentations:value});
    }

    handleMaxAttendees(event) {
        const value = event.target.value;
        this.setState({maxAttendees:value});
    }

    handleLocations(event) {
        const value = event.target.value;
        this.setState({location:value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" value={this.state.name} onChange={this.handleNameChange} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Room count" value={this.state.starts} onChange={this.handleStarts} required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Room count" value={this.state.ends} onChange={this.handleEnds} required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                <textarea className="form-control" value={this.state.description} onChange={this.handleDescription} id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="City" value={this.state.maxPresentations} required type="number" onChange={this.handleMaxPresentations} name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="City" required value={this.state.maxAttendees} type="number" onChange={this.handleMaxAttendees} name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select name="location" value={this.state.location} onChange={this.handleLocations} id="location" required className="form-select">
                  <option value="">Choose a Location</option>
                  {this.state.locations.map(location => {
                      return (
                          <option key={location.href} value={location.id}>
                              {location.name}
                          </option>
                      );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default ConferenceForm;


