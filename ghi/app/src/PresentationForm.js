import React from "react";

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conferences: [],
        }
    this.handlePresenterName = this.handlePresenterName.bind(this);
    this.handlePresenterEmail = this.handlePresenterEmail.bind(this);
    this.handleCompanyName = this.handleCompanyName.bind(this);
    this.handleSynopsis = this.handleSynopsis.bind(this);
    this.handleTitle = this.handleTitle.bind(this);
    this.handleConference = this.handleConference.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.companyName;
        console.log(data);
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.companyName;
        delete data.conferences 

        const conferenceUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            const cleared = {
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conferences: [],
            }
            this.setState(cleared);
        }   
    }

    handlePresenterName(event) {
        const value = event.target.value;
        this.setState({presenterName:value});
    }

    handlePresenterEmail(event) {
        const value = event.target.value;
        this.setState({presenterEmail:value});
    }

    handleCompanyName(event) {
        const value = event.target.value;
        this.setState({companyName:value});
    }

    handleSynopsis(event) {
        const value = event.target.value;
        this.setState({synopsis:value});
    }

    handleTitle(event) {
        const value = event.target.value;
        this.setState({title:value});
    }

    handleConference(event) {
        const value = event.target.value;
        this.setState({conference:value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences});
            
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterName} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresenterEmail} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyName} placeholder="company_name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitle} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Synopsis</label>
                <textarea className="form-control" onChange={this.handleSynopsis} id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select id="conference" onChange={this.handleConference} required className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences.map(conference => {
                      return (
                          <option key={conference.id} value={conference.id}>
                              {conference.name}
                          </option>
                      );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}


export default PresentationForm;