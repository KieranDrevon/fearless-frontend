console.log('Hey Brah');

// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload') // FINISH THIS
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload); // FINISH THIS

  // The payload is a JSON-formatted string, so parse it
  const payload =JSON.parse(decodedPayload); // FINISH THIS

  const addConference = payload.user.perms.includes('events.add_conference');
  const addLocation = payload.user.perms.includes('events.add_location');
  const addPresentation = payload.user.perms.includes('events.add_presentation');

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (addConference){
      const conferenceLinkTag = document.getElementById('new-conference-link');
      conferenceLinkTag.classList.remove('d-none');
  }


  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link

  if (addLocation){
    const locationLinkTag = document.getElementById('new-location-link');
    locationLinkTag.classList.remove('d-none');
}

  if (addPresentation){
    const presentationLinkTag = document.getElementById('new-presentation-link');
    presentationLinkTag.classList.remove('d-none');
  }

}