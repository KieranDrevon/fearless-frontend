function createCard(name, description, pictureUrl, newStarts, newEnds, locationName) {
    return `
      <div class="col-sm-3">
        <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class=" card card-footer">
            <p>${newStarts} - ${newEnds}  </p>
        </div>
        </div>
      </div>
    `;
  }

function createError(error){
    return `<div class="alert alert-dark" role="alert">
    Uh oh! Something went wrong, please refresh the page.
  </div>`
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const starts = new Date(details.conference.starts);
                const newStarts = starts.getMonth() + '/' + starts.getDate() + '/' + starts.getFullYear()
                const ends = new Date(details.conference.ends);
                const newEnds = ends.getMonth() + '/' + ends.getDate() + '/' + ends.getFullYear()
                const pictureUrl = details.conference.location.picture_url;
                const locationName = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, newStarts, newEnds, locationName);
                const column = document.querySelector('.row');
                column.innerHTML += html;
                        }
                    }
            }
    } catch (e) {
        const html = createError(e);
        const column = document.querySelector('.row');
        column.innerHTML += html;
        
    }
});
